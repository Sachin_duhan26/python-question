def solve(number, precision):

	start = 0
	end = number

	while (start <= end):
		mid = int((start + end) / 2)

		if (mid * mid == number):
			ans = mid
			break

		if (mid * mid < number):
			start = mid + 1
			ans = mid

        else:
			end = mid - 1

	increment = 0.1
	for i in range(0, precision):
		while (ans * ans <= number):
			ans += increment

		ans = ans - increment
		increment = increment / 10

	return ans


num = int(input('enter the number : '))
precision = int(input('enter the precision : '))
print(round(solve(num, precision), precision))
