try:
    a = int(input('enter the number 1 : '))
    b = int(input('enter the number 2 : '))
except ValueError:
    print('invalid integer character added!')

print('==============================')
print()
print('     welcome to the MENU     ')
print('OPTION 1 : "+" FOR ADDITION')
print('OPTION 2 : "-" FOR SUBSTRACTION')
print('OPTION 3 : "*" FOR MULTIPLICATION')
print('OPTION 4 : "/" FOR DIVISION')
print()
print('==============================')

try:
    c = input('choose you option : ')
except ValueError:
    print('only single character is allowed!')

if c is '+':
    print('Ans',a+b)
elif c is '-':
    print('Ans', a-b)
elif c is '*':
    print('Ans', a*b)
elif c is '/':
    print('Ans', a/b)
else:
    print('OPERATION NOT DEFINED!')
