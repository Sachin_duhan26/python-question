class Set:
    values = []
    def __init__(self,set=[]):
        for el in set:
            if el not in self.values:
                self.values.append(el)

    def print_elements(self):
        for el in self.values:
            print(el, end=" ")
        print()

    def cadinality(self):
        return len(self.values)
    
    def insert(self,el):
        if el not in self.values:
            self.values.append(el)
    
    def union(self,set):
        l = self.values
        for el in set:
            if el not in l:
                l.append(el)
        return l
    
    def intersection(self,set):
        n_l = self.values
        for el in self.values:
            if el in set:
                n_l.append(el)
        return n_l

a = Set()
a.insert(2)
a.insert(2)
a.insert(3)
a.insert(4)
a.insert(5)
a.print_elements()